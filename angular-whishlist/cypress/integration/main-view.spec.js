describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('Lista de Deseos en Angular');
        cy.get('p b').should('contain', 'HOLA es');
    });
});

describe('Fecha del sistema', () => {
    it('Se verifica la visualizacion de la fecha del sistema en pantalla', () => {
        cy.visit('http://localhost:4200');
        cy.get('p span').should('contain', 'Fecha');
    });
});

describe('Formulario de registro', () => {
    it('Se verifica la existencia de un formulario de registro de destino', () => {
        cy.visit('http://localhost:4200');
        cy.get('form div').should('contain', 'Nombre');
    });
});