export class DestinoViaje {
    private selected: boolean;
    public servicios: string[];
    public votes = 0;
    constructor(public id: number, public nombre: string, public url: string, public descripcion: string) {
        this.servicios = ['Piscina', 'Desayuno'];
    }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(isSelected: boolean) {
        this.selected = isSelected;
    }
    voteUp(): any {
        this.votes++;
    }

    voteDown(): any {
        this.votes--;
    }
}
