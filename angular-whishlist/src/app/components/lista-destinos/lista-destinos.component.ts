import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() itemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(private destinoApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.itemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
              .subscribe(d => {
                const fav = d;
                if (d != null) {
                  this.updates.push('Se ha elegido a ' + d.nombre);
                }
              });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }

  agregado(destino: DestinoViaje) {
   this.destinoApiClient.add(destino);
   this.itemAdded.emit(destino);
  }

  seleccionado(destinoViaje: DestinoViaje) {
    this.destinoApiClient.elegir(destinoViaje);
  }

}
