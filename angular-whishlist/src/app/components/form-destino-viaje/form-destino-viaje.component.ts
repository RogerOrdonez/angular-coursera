import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() itemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  resultados: string[];
  id = 0;

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.itemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizado(this.minLongitud)
      ])],
      url: [''],
      descripcion: ['']
    });
    this.fg.valueChanges.subscribe((form: any ) => {
      console.log('Cambio el formulario: ', form);
    });
  }

  ngOnInit() {
    const elemNombre = <HTMLInputElement> document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => this.resultados = ajaxResponse.response);
  }

  guardar(nombre: string, url: string, descripcion: string): boolean {
    const destino = new DestinoViaje(this.id++, nombre, url, descripcion);
    this.itemAdded.emit(destino);
    return false;
  }

  nombreValidator(control: FormControl): {[s: string]: boolean} {
    const longitud = control.value.toString().trim().length;
    if (longitud > 0 && longitud < 5) {
      return {invalidNombre: true};
    }
    return null;
  }

  nombreValidatorParametrizado(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const longitud = control.value.toString().trim().length;
      if (longitud > 0 && longitud < minLong) {
        return {minLongNombre: true};
      }
      return null;
    };
  }

}
