import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.model';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { AppState } from 'src/app/app.module';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    DestinosApiClient
  ],
})
export class DestinoDetalleComponent implements OnInit {
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor() { }

  ngOnInit() {
  }

}
